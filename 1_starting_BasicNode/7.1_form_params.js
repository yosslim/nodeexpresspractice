var http = require("http"),
    fs = require("fs"),
    parser = require("./7_params_parse.js");

    var p = parser.parse;


http.createServer(function(req, res){

    //Not:  habran dos request una del favicon y otra de la peticion normal

    if(req.url.indexOf("favicon.ico") > 0 ) {return;}

    fs.readFile("./indexWithForm.html",function(err, html){
        var html_string = html.toString();
    
        var variables = html_string.match(/[^\{\}]+(?=\})/g);
        var name = "Yorceli";

        var parametros = p(req);

        


        for(var i = variables.length - 1; i >= 0; i--){
          
            var variable = variables[i];
            //parametros[variable[i]]
            //parametros[]
            html_string = html_string.replace("{"+variables[i]+"}", parametros);
        };

        
        res.writeHead(200, {"Content-Type":"text/html"})
        res.write(html_string);
        res.end();
    });
}).listen(8080);
