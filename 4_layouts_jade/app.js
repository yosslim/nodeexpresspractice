var express = require("express");

//body-parser -> Lib para manejo de parametro
var bodyParser = require("body-parser");
var app = express();

//Primeros pasos con la conexion de la BD con mongoDB
var mongoose = require("mongoose"); //elegant mongodb objec modeling for node.js, es una api para hacer consultas a la bd de manera mas clara, tambien sirve como un driver para conectarme a mongoDB
var Schema = mongoose.Schema;

mongoose.connect("mongodb://localhost/fotos", { useNewUrlParser: true });

var userSchemaJSON ={
    email:String,
    password:String
};

var user_schema = new Schema(userSchemaJSON);
var User = mongoose.model("User",user_schema);


//bilding Middleware que express tiene para archivos estatico
//se asumira que existe una carpeta public y assets donde estaran los arch(img...etc)

// app.use("estatico",express.static('public'));   => Aqui afuerza debe existir esta carpeta (estatico) y en el path debo de ponerla

app.use(express.static('public'));
app.use(express.static('assets'));

//Aqui usaremos el middleware para buscar los archivos y extraerlos
app.use(bodyParser.json()); //para peticiones application/json
app.use(bodyParser.urlencoded({extended: true})); // si el extender es true podemos hacer parsin de algo mas complejo como arreglos, etc, con falso no.

app.set("view engine", "jade");

//Middlewares normales de la aplicacion, por defecto de express

app.get("/", function(req, res){

    res.render("index");
})

app.get("/login", function(req, res){

    //Para verificar que los datos se guardaron en BD

    User.find(function(err,doc){
        console.log(doc);
        res.render("login");
    });
})

app.post("/users", function(req, res){
   //Aqui vamos a guardar los datos a la BD que vienen del formulario
   var user = new User({email: req.body.email, password: req.body.password});

   user.save(function(){
       res.send("Guardamos tus datos");
   });
   
})



app.listen(8080);

//Nota: usaremos jade es un motor de vistar como razor en MVC .net,
        //es uno de los mas populares, tambien existen ejs, etc 