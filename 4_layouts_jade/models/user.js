var mongoose = require("mongoose"); //elegant mongodb objec modeling for node.js, es una api para hacer consultas a la bd de manera mas clara, tambien sirve como un driver para conectarme a mongoDB
var Schema = mongoose.Schema;

/*Tipos de datos que acepta mongoDB
    String, Number, Date, Buffer, Boolean, Mixed, Objectid, Array
*/

var user_schema = new Schema({
    name: String,
    userName: String,
    password: String,
    age: Number,
    email: String,
    date_of_birth: Date
});
