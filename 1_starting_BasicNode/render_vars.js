var http = require("http"),
    fs = require("fs");


http.createServer(function(req, res){
    fs.readFile("./indexWithParameter.html",function(err, html){
        var html_string = html.toString();

        //regular expresion: check in the html file {x} 
        var variables = html_string.match(/[^\{\}]+(?=\})/g);
        var name = "Yorceli";
        // variables ['nombre']
        // variables ['name']
        for(var i = variables.length - 1; i >= 0; i--){

            //Lo ejecutamos como codigo de JS para obtener el valor de dicha variable
            var value = eval(variables[i]);
            // reemplazar el contenido con llaves {x} por su valor correspondene
            html_string = html_string.replace("{"+variables[i]+"}", value);
        };

        //send the content
        res.writeHead(200, {"Content-Type":"text/html"})
        res.write(html_string);
        res.end();
    });
}).listen(8080);
