var express = require("express");

var app = express();
app.set("view engine", "jade");

// Verbos Http => GET / POST / PUT / PATCH / OPTIONS / HEADERS / DELETE
// ARQUITECTURA REST

/*-----------------------------------------*/

//manejo de get (obtener)
//get es para hacer solicitudes al servidor, por default la raiz es "/" y tratara de acceder a esa ruta

app.get("/", function(req, res){
    // res.send("hello world");  // el metodo send es particular de express y cierra la conexion

    res.render("index");
})

//Este metodo get, entra a la ruta que se le pase una var como par
app.get("/:nombre", function(req, res){
    // res.send("hello world");  // el metodo send es particular de express y cierra la conexion

    res.render("form", {nombre: req.params.nombre});
})

//Post es para uso de valores a traves de formularios (Poner)

app.post("/", function(req, res){
    // res.send("hello world");  // el metodo send es particular de express y cierra la conexion

    res.render("form");
})

app.listen(8080);

//Nota: usaremos jade es un motor de vistar como razor en MVC .net,
        //es uno de los mas populares, tambien existen ejs, etc 