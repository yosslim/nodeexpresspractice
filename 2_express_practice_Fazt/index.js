const express = require("express");
const app = express();
const morgan = require("morgan");

//settings
app.set('appName', 'My first server');
app.set('views', __dirname + '/views');

app.set('view engine', 'ejs');

//middlewares

//usando el modulo morgan (trabaja con los logins)

//types of request

// app.use(morgan('dev'));

// app.use(morgan('short'));

app.use(morgan('combined'));

//Manuales

app.use(function(req, res, next){
  console.log('request url:' + req.url);
  next();
});

app.use(function(req, res, next){
  console.log('ha pasado por esta funcion');
  next();
});

//routing

app.get('/', (req, res) => {
  res.render('index.ejs');
});

app.get('/login', (req, res) => {
  res.render('login');
});

app.get('*', (req, res) => {
  res.end('File not found');
});

app.listen(3000, function(){
  console.log("servidor funcionando!");
  console.log("Nombre de la APP: ", app.get('appName'));
});