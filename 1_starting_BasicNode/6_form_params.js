var http = require("http"),
    fs = require("fs");


http.createServer(function(req, res){

    //Not:  habran dos request una del favicon y otra de la peticion normal

    if(req.url.indexOf("favicon.ico") > 0 ) {return;}

    fs.readFile("./indexWithForm.html",function(err, html){
        var html_string = html.toString();
        var arreglo_parametros = [], parametros = {};

        var variables = html_string.match(/[^\{\}]+(?=\})/g);
        var name = "Yorceli";

        if(req.url.indexOf("?") > 0){
            // /?name=Yorceli
            var url_data = req.url.split("?");
            arreglo_parametros = url_data[1].split("&");
            // [name=Yorce,data=anything]
        }

        for(var i = arreglo_parametros.length - 1; i >= 0; i--){
           
            var parametro = arreglo_parametros[i];            
            //name=Yorceli
            var param_data = parametro.split("=");
            //[name,Yorceli]
            parametros = [param_data[0]] = param_data[1];
            //{name:Yorceli}
        };

        for(var i = variables.length - 1; i >= 0; i--){
          
            var variable = variables[i];
            //parametros[variable[i]]
            //parametros[]
            html_string = html_string.replace("{"+variables[i]+"}", parametros);
        };

        
        res.writeHead(200, {"Content-Type":"text/html"})
        res.write(html_string);
        res.end();
    });
}).listen(8080);
