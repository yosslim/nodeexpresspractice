function parse(req){
    var arreglo_parametros = [], parametros = {};

    if(req.url.indexOf("?") > 0){
        // /?name=Yorceli
        var url_data = req.url.split("?");
        arreglo_parametros = url_data[1].split("&");
        // [name=Yorce,data=anything]
    }

    for(var i = arreglo_parametros.length - 1; i >= 0; i--){
           
        var parametro = arreglo_parametros[i];            
        //name=Yorceli
        var param_data = parametro.split("=");
        //[name,Yorceli]
        parametros = [param_data[0]] = param_data[1];
        //{name:Yorceli}
    };

    return parametros
}

module.exports.parse = parse;